[[_TOC_]]

# TL;DR -- No time for reading, I Want My Own VPN right now! What should I do?

1. In the AWS console, create an AWS IAM user called `mytmpvpn`, give it the `programmatic access` type, and gives it the following permissions using the `attach existing policies`: `AmazonEC2FullAccess, AWSCloudFormationFullAccess`.
2. From here, everything happen on your local host.
3. `git clone https://gitlab.com/pierre.vigneras/mytmpvpn`
4. `sudo apt install iproute2 openssh-client procps openssl openvpn awscli`
5. Create the `mytmpvpn` profile and download the `.csv` creditentials.
6. In the AWS config file `~/.aws/config with the correct region
  
   ```
   [profile mytmpvpn]
    region = <YOUR_REGION>
   ```
7. In the AWS credentials file `~/.aws/credentials with the correct credentials 
   ```
   [mytmpvpn]
    aws_access_key_id = <YOUR_AWS_ACCESS_KEY_ID>
    aws_secret_access_key = <YOUR_AWS_SECRET_ACCESS_KEY>
   ```
5. `./mytmpvpn-provision` provisions your own VPN stack
6. `(cd /tmp/mytmpvpn; sudo ./connect)` connects to your own VPN
7. Enjoy your own VPN, connect where you need, for a small cost (depending on your usage)
8. `(cd /tmp/mytmpvpn; sudo ./disconnect)` disconnecs from your own VPN (but the server remains)
9. `./mytmpvpn-deprovision` deprovisions everything (the server is gone)

# What's this?

Need to access a given web-site from another country for a short period, but don't want to subscribe to a monthly VPN solution? Or you just want to pay for what you use? `MyTmpVPN` provides a simple temporary VPN. Ideally: 

1. you install it (should take 2 mn on any Linux based distribution);
2. you configure it (should take 5 mn on any Linux based distribution);
3. you provision resources in the AWS cloud (should take about 2 mn);
4. you connect to your own provisioned resources using a small script (should take 30 s)
5. enjoy, and profit.
6. you can connect/disconnect from your own VPN at will, it's your own!
7. eventually, you will deprovision your own resources from AWS
8. finally, you pay the bill, but only for what you've consumed.

Most of the time, instead of disconnecting, I just deprovision, so cost remains low.

# How much does it cost?

It actually depends on:
* your usage, because you only pay for what you use;
* [AWS pricing](https://calculator.aws/#/), because `MyTmpVPN` is based on AWS for now: it uses only 1 EC2 small instance.
* In my own experience, using this to watch the FIFA 2018 world cup, it cost me less than 5 CAD/month.

# Cool, how do I use it?

## One-Off: Install all required stuff (20 minutes) 

1. You need a Linux distro. I'm currently using [Ubuntu](https://ubuntu.com/) but there is nothing really specific here. It should work with any Linux distro. You might need to adapt the client-side script, but nothing should be hard.
2. You also need the following softwares (commands are given for Ubuntu, please adapt to your own distro):
    
    * [`iproute2`](https://wiki.linuxfoundation.org/networking/iproute2): `sudo apt install iproute2`
    * [`sudo`](http://www.sudo.ws/): `sudo apt install sudo`
    * [`tar`](https://www.gnu.org/software/tar/): `sudo apt install tar`
    * [`openssh`](http://www.openssh.com/): `sudo apt install openssh-client`
    * [`procps`](https://gitlab.com/procps-ng/procps): `sudo apt install procps`
    * [`openssl`](https://www.openssl.org/): `sudo apt install openssl`
    * [`openvpn`](https://openvpn.net/): `sudo apt install openvpn`
    * [`aws`](https://aws.amazon.com/cli/): `sudo apt install awscli`

3. Finally, you need an AWS account: https://aws.amazon.com/ because you're going to provision your own resources.
   If you're new to AWS, you're lucky, you'll be free-tier eligible, and probably won't pay anything for 1 year! It will depend on your usage of course, see [here for details](https://aws.amazon.com/free/free-tier-faqs/#:~:text=Services%20with%20a%2012%2Dmonth,they%20are%20an%20AWS%20customer.)
4. [optional but **highly recommended**] Configure your AWS account, so you don't use your AWS root account! If you're new to AWS this might sound intimidating, but there is nothing to worry about, really. Just follow the process described [here](https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html). This is a one-off task, you won't have to do this anymore.
5. Log in to AWS console and go to the IAM service, we are going to create a specific user that will be allowed to create AWS resources:

   a. Add User -> enter `mytmpvpn` as the name and check `Programmatic access`;
   b. Click on `Attach existing policy directly`: 
      + search for `AmazonEC2FullAccess` and tick the checkbox, 
      + search for `AWSCloudFormationFullAccess` and tick the checkbox
      + click `Next`;
   c. Click on `Review`, verify, and click on `Add user`.
   d. **Important step**: download the credentials, they won't be recoverable after this step. Store them in a secure place. Personally I use the awesome [standard Unix password manager](https://www.passwordstore.org/) project to store all my password.
6. Determine the region you want to connect to. That is the region you're going to provision some AWS resources, to create your own private VPN, and from which your web requests will pretend coming from. Say, you're in Sweden, and wants to connect to a Brazil web site, you will specify: `sa-east-1`. The list of all AWS regions is available on the AWS console (top right corner). For more details on AWS infrastructure: https://www.infrastructure.aws/. 
7. Configure your AWS CLI Profile so it uses the above credentials. Create a file `~/.aws/config` containing the following entries (adapt with your own credentials):

   ```
    [profile mytmpvpn]
    region = <YOUR_REGION>
    aws_access_key_id = <YOUR_AWS_ACCESS_KEY_ID>
    aws_secret_access_key = <YOUR_AWS_SECRET_ACCESS_KEY>
   ```


## On-demand: connect to your own VPN resources? (2 minutes)

1. First you'll have to provision new AWS resources for your VPN. This should be as simple as: `./mytmpvpn-provision`. It will take some time (1-2 minutes), but the script won't exit until this is finished. The script will also create a new folder `/tmp/mytmpvpn-client` containing the ad-hoc script to connect to your own VPN. Alternatively, you can monitor the progress of the provisioning in the AWS console: go to AWS -> CloudFormation -> You should see a stack called `mytmpvpn`.
2. The script will display the ad-hoc folder created for you that contains the commands that you should use to connect/disconnect to/from your own VPN resources.

* Connecting should be as simple as: `(cd /tmp/mytmpvpn; sudo ./connect)`
* Disconnecting should be as simple as: `(cd /tmp/mytmpvpn; sudo ./disconnect)`
* Don't forget to deprovision your VPN resources to reduce cost: `./mytmpvpn-deprovision`
    

# How does it work?

You guess! Read the code. ;-)

# Frequently Asked Questions

See [FAQ](FAQ.md) file
