[[_TOC_]]

# Provisioning

## How to debug?

Change in the `mytmpvpn-provision` script the first line:

```bash
#!/bin/bash -e
```

to the following:

```bash
#!/bin/bash -ex
```

This will outputs the different commands that are actually executed, with all their parameters. For more details, see the [`bash` manual](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html).

## How to monitor the provisioning progress?

Connect to your AWS console, select your correct region, and the CloudFormation service. There you should see your stack, activities and in case of errors, the actual reason. 

Alternatively, you might use AWS CloudWatch and see look at the logs.

## An error occurred (AlreadyExistsException) when calling the CreateStack operation: Stack [mytmpvpn] already exists

You already have a cloudformation `mytmpvpn` stack. This is usually because you just deprovisioned using the `mytmpvpn-deprovision` command, but it takes some time to fully deprovision. Try again after a short time (~2 minutes).

# Connect

## How can I monitor my own VPN resources

Log to your AWS console, select your region, and go to EC2. There you should see your EC2 instance from the `mytmpvpn` stack, and use all AWS tools for monitoring.


# Disconnect

## Error: RTNETLINK answers: File exists

Not sure why this is happening for now. But it does not lead to any issue AFAIK.

# Deprovisioning