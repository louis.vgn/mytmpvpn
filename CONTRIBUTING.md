# How to contribute?

1. Fork the project: https://gitlab.com/pierre.vigneras/mytmpvpn/-/forks/new
2. Clone the project in your own local workspace
3. Modify/Tests (sorry no unit-tests for now)
4. Create a merge requests: https://docs.gitlab.com/ee/user/project/merge_requests/

